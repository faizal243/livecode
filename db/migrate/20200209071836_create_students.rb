class CreateStudents < ActiveRecord::Migration[6.0]
  def up
    create_table :students do |t|
      t.string :nama 
      t.string :username
      t.integer :age
      t.string :class
      t.string :address
      t.string:city
      t.string :nik

      t.timestamps
    end
  end
end
