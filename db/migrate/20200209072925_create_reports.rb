class CreateReports < ActiveRecord::Migration[6.0]
  def up
    create_table :reports do |t|
      t.string :tittle
      t.string :hasil
      t.string :mapel
      t.integer :teacher_id
      t.string :date

      t.timestamps
    end
  end
end
