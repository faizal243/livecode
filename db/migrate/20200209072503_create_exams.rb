class CreateExams < ActiveRecord::Migration[6.0]
  def up
    create_table :exams do |t|
      t.string :tittle
      t.string :mapel
      t.string :duration
      t.integer :nilai
      t.string :aktif
      t.integer :level
      t.integer :student_id

      t.timestamps
    end
  end
end
