class CreateTeachers < ActiveRecord::Migration[6.0]
  def up
    create_table :teachers do |t|
      t.string :nik
      t.string :name
      t.string :age
      t.string :kelas
      t.string :mapel

      t.timestamps
    end
  end
end
