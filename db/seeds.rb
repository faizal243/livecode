# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
 

 Student.create(nama: 'moch.faizal', username: 'faizal', age: 12, kelas: 'rpl-4', address: 'cisarua.bogor', city: 'bogor',  nik: '1111111')
 Student.create(nama: 'putri partiwi', username: 'putri', age: 12, kelas: 'rpl-4', address: 'cisarua.bogor', city: 'bogor',  nik: '2222222')